//============================================================================
// Name        : projekt_koncowy_testy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "gtest.h"
#include "BTS.h"
#include "MSC.h"
#include "Phone.h"
#include "BSC.h"
#include "Charger.h"
#include <vector>

using namespace std;

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

TEST(add_to_map_of_bts,sth){
	BTS one(2);
	BTS two(5);
	BTS three(49);

	BSC first;
	first.add_to_map_of_bts(one);
	first.add_to_map_of_bts(two);
	first.add_to_map_of_bts(three);
	EXPECT_EQ(3,first.return_nr_of_bts());

}

TEST(find_nearest_bts,sth){

	BTS one(4);
	BTS two(435);
	BTS three(400);

	BSC first;
	first.add_to_map_of_bts(one);
	first.add_to_map_of_bts(two);
	first.add_to_map_of_bts(three);

	first.find_nearest_bts(2);
	EXPECT_EQ(one.getLocation(),first.find_nearest_bts(2)->getLocation());


}



//TEST(Tescik, costam) {
//		BTS testowy(312);
//		Phone* testwsk1;
//		Phone* testwsk2;
//		Phone* testwsk3;
//
//		testowy.register_phone(testwsk1);
//		EXPECT_EQ(true,testowy.register_phone(testwsk2));
//	}

TEST(TestingCharger, chargingZero){
	Charger testowy(Charger::chargerType::N);
	Phone testphone(4,444666,0,Phone::brand::htc,Phone::phone_type::desire);
//	Phone* testphone1;

EXPECT_EQ(100, testowy.chargeBattery(&testphone));

}
TEST(TestingCharger, chargingMinus){
	Charger testowy(Charger::chargerType::N);
	Phone testphone(4,444666,-20,Phone::brand::htc,Phone::phone_type::desire);
//	Phone* testphone1;

EXPECT_EQ(100, testowy.chargeBattery(&testphone));

}
TEST(TestingCharger, chargingFifty){
	Charger testowy(Charger::chargerType::N);
	Phone testphone(4,444666,50,Phone::brand::htc,Phone::phone_type::desire);
//	Phone* testphone1;

EXPECT_EQ(100, testowy.chargeBattery(&testphone));

}
TEST(TestingCharger, chargingAboveHundred){
	Charger testowy(Charger::chargerType::N);
	Phone testphone(4,444666,200,Phone::brand::htc,Phone::phone_type::desire);
//	Phone* testphone1;

EXPECT_NE(100, testowy.chargeBattery(&testphone));

}


TEST(TestingCharger, chargingFull){
	Charger testowy(Charger::chargerType::N);
	Phone testphone(4,444666,100,Phone::brand::htc,Phone::phone_type::desire);
//	Phone* testphone1;

EXPECT_EQ(100, testowy.chargeBattery(&testphone));

}
TEST(TestingCompatibility, compatible){
	Charger testowy(Charger::chargerType::N);
	Phone testphone(4,444666,100,Phone::brand::nokia,Phone::phone_type::n3310);
	EXPECT_TRUE(testowy.compatibility(&testphone));
}

TEST(TestingCompatibility, compatibleWithE){
	Charger testowy(Charger::chargerType::E);
	Phone testphone(4,444666,100,Phone::brand::nokia,Phone::phone_type::n3310);
	EXPECT_TRUE(testowy.compatibility(&testphone));
}
TEST(TestingCompatibility, notCompatibleWithE){
	Charger testowy(Charger::chargerType::E);
	Phone testphone(4,444666,100,Phone::brand::iPhone,Phone::phone_type::n3310);
	EXPECT_FALSE(testowy.compatibility(&testphone));
}


class Objects : public testing::Test{
protected:
	virtual void SetUp(){
		testphone = Phone(2, 3, 4, Phone::brand::htc, Phone::phone_type::desire);
		bts1 = BTS(2);
		bts2 = BTS(1);
		bts3 = BTS(4);
		bts4 = BTS(3);
		bts5 = BTS(6);
		bts6 = BTS(5);

		bsc1 = BSC();
		bsc2 = BSC();

		msc1 = MSC();
	}
	Phone testphone;
	BTS bts1;
	BTS bts2;
	BTS bts3;
	BTS bts4;
	BTS bts5;
	BTS bts6;
	BSC bsc1;
	BSC bsc2;
	MSC msc1;
};

TEST_F(Objects, Adding_BSC_empty){
	auto &test=msc1.getBscContainer();

EXPECT_EQ(0, test.size());
}

TEST_F(Objects, Adding_BSC_first){
	auto &test=msc1.getBscContainer();
	msc1.addBSC(bsc1);

EXPECT_EQ(1, test.size());
}

TEST_F(Objects, Adding_BSC){
	auto &test=msc1.getBscContainer();
	msc1.addBSC(bsc1);
	msc1.addBSC(bsc2);

EXPECT_EQ(2, test.size());
}

//TEST_F(Objects, Find_closest_BTS_first_try){
//
//	bsc1.add_to_map_of_bts(bts1);
//	bsc1.add_to_map_of_bts(bts2);
//	bsc1.add_to_map_of_bts(bts3);
//
//	bsc2.add_to_map_of_bts(bts4);
//	bsc2.add_to_map_of_bts(bts5);
//	bsc2.add_to_map_of_bts(bts6);
//
//	msc1.addBSC(bsc1);
//	msc1.addBSC(bsc2);
//
//	msc1.find_closest_BTS(2);
//
//EXPECT_EQ(bts1.getLocation(), msc1.find_closest_BTS(2)->getLocation());
//}

class BTSTestFixture : public testing::Test{
protected:
	virtual void SetUp() {
		testBTS = BTS(10);
		testPhone = Phone(10, 123456, 80, Phone::brand::nokia, Phone::phone_type::n3310);
		testPhone2 = Phone(20, 654321, 50, Phone::brand::htc, Phone::phone_type::desire);
	}
	virtual void TearDown(){}

	BTS testBTS;
	Phone testPhone;
	Phone testPhone2;
};

TEST_F (BTSTestFixture, BTSConstructedCorrectly){
	EXPECT_EQ(10, testBTS.getLocation());
}
TEST_F (BTSTestFixture, FunctionRegisterPhone){
	Phone testPhone3;
	Phone testPhone4;
	Phone testPhone5;
	Phone testPhone6;
	testBTS.register_phone(&testPhone3);
	testBTS.register_phone(&testPhone4);
	testBTS.register_phone(&testPhone5);
	testBTS.register_phone(&testPhone6);

	EXPECT_TRUE(testBTS.register_phone(&testPhone));
	EXPECT_FALSE(testBTS.register_phone(&testPhone2)); // false because BTS is full
}
TEST_F (BTSTestFixture, FunctionUnregisterPhone){
	EXPECT_EQ(0, testBTS.getPhonesMap().size());

	testBTS.register_phone(&testPhone);
	EXPECT_EQ(1, testBTS.getPhonesMap().size());

	testBTS.unregister_phone(654321);
	EXPECT_EQ(1, testBTS.getPhonesMap().size());

	testBTS.unregister_phone(123456);
	EXPECT_EQ(0, testBTS.getPhonesMap().size());
}
