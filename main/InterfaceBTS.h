/*
 * InterfaceBTS.h
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#ifndef INTERFACEBTS_H_
#define INTERFACEBTS_H_

class Phone;

class InterfaceBTS {
public:
	InterfaceBTS();
	virtual ~InterfaceBTS();
	virtual bool register_phone(Phone* phone)=0;
	virtual void unregister_phone(Phone* phone)=0;

};

#endif /* INTERFACEBTS_H_ */
