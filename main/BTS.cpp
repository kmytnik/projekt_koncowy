/*
 * BTS.cpp
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#include "BTS.h"
#include "Phone.h"
#include <map>

BTS::BTS(int location) {
	this->location = location;

}

BTS::BTS(){

}

BTS::~BTS() {
	// TODO Auto-generated destructor stub
}

bool BTS::register_phone(Phone* phone) {
	if(phones_map.size()<sizeof_phones_map){
	phones_map.insert (std::pair <int,Phone*> (phone->number_phone,phone) );
	return true;
	}
	else {
		std::cerr<<"BTS overloaded...switching to available BTS"<<std::endl;
		return false;
	}
}

void BTS::unregister_phone(Phone *phone) {
	phones_map.erase(phone->number_phone);
}

int BTS::sizeOfBts() {
	return phones_map.size();
}
