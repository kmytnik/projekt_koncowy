/*
 * InterfaceBSC.h
 *
 *  Created on: 23.06.2017
 *      Author: APPDOM
 */

#ifndef INTERFACEBSC_H_
#define INTERFACEBSC_H_

class BTS;
class InterfaceBSC {
public:
	InterfaceBSC();
	virtual ~InterfaceBSC();

	virtual BTS* find_nearest_bts(int phone_location)=0;
	virtual void add_to_map_of_bts(BTS)=0;

		virtual int return_nr_of_bts()=0;

};

#endif /* INTERFACEBSC_H_ */
