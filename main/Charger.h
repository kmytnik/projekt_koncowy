/*
 * Charger.h
 *
 *  Created on: 22.06.2017
 *      Author: RENT
 */

#ifndef CHARGER_H_
#define CHARGER_H_
#include "InterfaceCharger.h"
#include "Phone.h"
#include <iostream>

class Charger: public InterfaceCharger {

public:
	enum chargerType {//dany telefon bedzie kompatybilny z ta sama i nastepna ladowarka
		E, N, H, I
	};

	chargerType type;
	int powerManager();
	bool compatibility(Phone *somePhone) override;
	int chargeBattery(Phone *somePhone) override;

	Charger();
	Charger(chargerType type);
	virtual ~Charger();
};

#endif /* CHARGER_H_ */
