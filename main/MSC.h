/*
 * MSC.h
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#ifndef MSC_H_
#define MSC_H_

#include "Interface.h"
#include <vector>
#include "BSC.h"


class BSC;

class MSC: public Interface {
public:
	MSC();
	virtual ~MSC();

	void addBSC(BSC) override;
	void register_phone(Phone* phone) override;
	BTS* find_closest_BTS(Phone* phone) override;
	const std::vector<BSC>& getBscContainer() const {
		return BSCContainer;
	}

private:
	std::vector<BSC> BSCContainer;
};

#endif /* MSC_H_ */
