/*
 * BSC.cpp
 *
 *  Created on: 23.06.2017
 *      Author: APPDOM
 */

#include "BSC.h"

BSC::BSC() {
	// TODO Auto-generated constructor stub

}

BSC::~BSC() {
	// TODO Auto-generated destructor stub
}

BTS* BSC::find_nearest_bts(int phone_location) {

	BTS* result=&map_of_bts.begin()->second;
	int min=1000;
	for (auto it=map_of_bts.begin(); it!=map_of_bts.end(); ++it) {


		if(abs(phone_location-it->first)<min) {min=abs(phone_location-it->first);
		result=&it->second;}


	}
	return result;
}

void BSC::add_to_map_of_bts(BTS ONE) {

	  map_of_bts.insert ( std::pair<int,BTS>(ONE.getLocation(),ONE) );

}

int BSC::return_nr_of_bts() {

	return map_of_bts.size();
}
