/*
 * BSC.h
 *
 *  Created on: 23.06.2017
 *      Author: APPDOM
 */

#ifndef BSC_H_
#define BSC_H_
#include<iostream>
#include<map>
#include "InterfaceBSC.h"
#include "BTS.h"
#include<stdlib.h>

class BTS;
class MSC;
class BSC : public InterfaceBSC
{
	MSC* msc;
	std::map<int,BTS>map_of_bts;

public:
	BSC();
	virtual ~BSC();
	virtual BTS* find_nearest_bts(int phone_location) override;
	virtual void add_to_map_of_bts(BTS) override;
	virtual int return_nr_of_bts() override;

};

#endif /* BSC_H_ */
