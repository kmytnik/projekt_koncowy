/*
 * Interface.h
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#ifndef INTERFACE_H_
#define INTERFACE_H_

class BTS;
class BSC;
class Phone;

class Interface {
public:
	Interface();
	virtual ~Interface();
	virtual void addBSC(BSC)=0;
	virtual void register_phone(Phone* phone)=0;
	virtual BTS* find_closest_BTS(Phone* phone)=0;
};

#endif /* INTERFACE_H_ */
