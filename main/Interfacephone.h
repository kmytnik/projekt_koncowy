/*
 * Interfacephone.h
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#ifndef INTERFACEPHONE_H_
#define INTERFACEPHONE_H_

class Phone;

class Interface_phone {
public:
	Interface_phone();
	virtual ~Interface_phone();

	virtual void charge (int battery_level)=0;
	virtual void connectToNetwork ()=0;
	virtual void disconnectToNetwork (int location)=0;

};


#endif /* INTERFACEPHONE_H_ */
