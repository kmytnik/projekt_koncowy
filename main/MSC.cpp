/*
 * MSC.cpp
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#include "MSC.h"
#include "BTS.h"
#include "BSC.h"
#include "Phone.h"

#include <iostream>

MSC::MSC() {

}

MSC::~MSC() {

}

void MSC::addBSC(BSC bsc) {
	BSCContainer.push_back(bsc);
}

void MSC::register_phone(Phone* phone) {
	BTS* local=find_closest_BTS(phone);
	local->register_phone(phone);
	phone->bts=local;
}

BTS* MSC::find_closest_BTS(Phone* phone) {
	int location=phone->getLocation();
	BTS *final = BSCContainer[0].find_nearest_bts(location);
	BTS *temporary;
	for(auto i = BSCContainer.begin()+1; i!=BSCContainer.end(); i++){
		temporary=i->find_nearest_bts(location);
		if(temporary->getLocation()-location<final->getLocation()-location){
			final=temporary;
		}

	}

	return final;
}
