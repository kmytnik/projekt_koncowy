/*
 * InterfaceCharger.h
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#ifndef INTERFACECHARGER_H_
#define INTERFACECHARGER_H_
class Phone;
class Charger;

class InterfaceCharger {
public:
	virtual int powerManager()=0;
	virtual bool compatibility(Phone *somePhone)=0;
	virtual int chargeBattery(Phone *somePhone)=0;
	InterfaceCharger();
	virtual ~InterfaceCharger();
};

#endif /* INTERFACECHARGER_H_ */
