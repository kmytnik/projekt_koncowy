/*
 * Phone.h
 *
 *  Created on: 22.06.2017
 *      Author: RENT
 */

#ifndef PHONE_H_
#define PHONE_H_
#include "Interfacephone.h"

class MSC;
class BTS;

class Phone: public Interface_phone {

public:
	int location;
	int battery_level;
	int number_phone;
	enum brand {nokia,ericsson,htc,iPhone};
	enum phone_type {n3310,T280i,desire,c5};
	brand Brand_type;
	phone_type Phone_type;
	MSC* msc;
	BTS* bts;


	void charge(int battery_level) override;
	void connectToNetwork() override;
	void disconnectToNetwork(int location) override;

	Phone ();
	Phone(int location, int number_phone, int battery_level, brand Brand_type, phone_type Phone_type);
	virtual ~Phone ();

	int getLocation() const {
		return location;
	}
};


#endif
