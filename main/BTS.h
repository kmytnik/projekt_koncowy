/*
 * BTS.h
 *
 *  Created on: 23.06.2017
 *      Author: RENT
 */

#ifndef BTS_H_
#define BTS_H_
#include <iostream>
#include <map>
#include "InterfaceBTS.h"
#include "Phone.h"

class BSC;
class Phone;

class BTS : public InterfaceBTS  {
public:

	BTS(int location);
	BTS();
	virtual ~BTS();
	bool register_phone(Phone *phone) override;       // metoda zwraca true i dodaje telefon lub false i podaje komunikat
	void unregister_phone(Phone *phone) override;
	void setClosestBsc(BSC*& closestBsc) {
		closest_BSC = closestBsc;
	}

	int sizeOfBts();


	int getLocation()  {
		return location;
	}

	const std::map<int, Phone*>& getPhonesMap() const {return phones_map;}


private:
	int location;
	static const int sizeof_phones_map = 5;   //rozmiar kontenera BTS na telefony
	BSC* closest_BSC;
	std::map<int,Phone*> phones_map;
};

#endif /* BTS_H_ */
